import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import parser.*;

public class NPJListenerImpl extends NPJBaseListener {
    private static final int NULL = 0;

    private CollectorImpl collector;
    private int[] heap;
    private Map<String, ObjectMetadata> variables;

    public NPJListenerImpl(CollectorImpl collector, int[] heap) {
        this.collector = collector;
        this.heap = heap;
        variables = new HashMap<>();
    }

    @Override
    public void exitVarDeclSWithNull(NPJParser.VarDeclSWithNullContext ctx) {
        variables.put(ctx.STRING().getText(), new ObjectMetadata(NULL, Type.S));
    }

    @Override
    public void exitVarDeclSWithString(NPJParser.VarDeclSWithStringContext ctx) {
        String varName = ctx.STRING(0).getText();
        String value = ctx.STRING(1).getText();
        allocateNewString(varName, value);
    }
    @Override
    public void exitVarDeclT(NPJParser.VarDeclTContext ctx) {
        checkHeap(4);

        int allocPtr = collector.getAllocPtr();
        variables.put(ctx.STRING().getText(), new ObjectMetadata(allocPtr, Type.T));
        heap[allocPtr++] = Type.T.code;
        heap[allocPtr++] = NULL;
        heap[allocPtr++] = NULL;
        heap[allocPtr++] = 0;
        collector.setAllocPtr(allocPtr);
    }

    @Override
    public void exitAssignment(NPJParser.AssignmentContext ctx) {
        String lValue = ctx.lvalue().getText();
        String rValue = ctx.rvalue().getText();
        if(lValue.contains(".")) {
            int lPtr = resolvePtr(lValue);
            try {
                heap[lPtr] = Integer.parseInt(rValue);
            } catch (NumberFormatException e) {
                heap[lPtr] = resolveRValuePtr(rValue);
            }
        } else if(variables.get(lValue).type == Type.T) {
            variables.get(lValue).ptr = resolveRValuePtr(rValue);
        } else if(rValue.startsWith("\"")) {
            allocateNewString(lValue, rValue.substring(1, rValue.length() - 1));
        } else {
            int newPtr = "NULL".equals(rValue) ? NULL : variables.get(rValue).ptr;
            variables.get(lValue).ptr = newPtr;
        }
    }

    private int resolvePtr(String value) {
        String[] valueParts = value.split("\\.");
        Iterator<String> valuePartsIt = Arrays.asList(valueParts).iterator();
        int ptr = variables.get(valuePartsIt.next()).ptr;
        while(valuePartsIt.hasNext()) {
            String part = valuePartsIt.next();
            if("f1".equals(part)) {
                ptr += 1;
            } else if("f2".equals(part)) {
                ptr += 2;
            } else {//"data"
                ptr += 3;
            }

            if(valuePartsIt.hasNext()) {
                ptr = heap[ptr];
            }
        }
        return ptr;
    }

    private int resolveRValuePtr(String value) {
        if("NULL".equals(value)) {
            return NULL;
        }
        int newRPtr = resolvePtr(value);
        return value.contains(".") ? heap[newRPtr] : newRPtr;
    }

    @Override
    public void exitPrintStringInQuotes(NPJParser.PrintStringInQuotesContext ctx) {
        NPJ.print(ctx.STRING().getText());
    }

    @Override
    public void exitPrintVarDeclS(NPJParser.PrintVarDeclSContext ctx) {
        int position = variables.get(ctx.STRING().getText()).ptr;
        NPJ.print(getString(position));
    }

    @Override
    public void exitCollect(NPJParser.CollectContext ctx) {
        NPJ.collect(heap, collector, (Map) variables);
    }

    @Override
    public void exitHeapAnalyze(NPJParser.HeapAnalyzeContext ctx) {
        List<Integer> typeTVariables = new ArrayList<Integer>();
        List<String> typeSVariables = new ArrayList<String>();
        Set<Integer> visitedPtrs = new HashSet<Integer>();
        for(Map.Entry<String, ObjectMetadata> entry : variables.entrySet()) {
            ObjectMetadata value = entry.getValue();

            if(varShouldNotBeAdded(value.ptr, visitedPtrs)) {
                continue;
            }

            visitedPtrs.add(value.ptr);
            if(value.type == Type.S) {
                typeSVariables.add(getString(value.ptr));
            } else {
                typeTVariables.add(heap[value.ptr + 3]);
                traverseFieldsOfVarT(value.ptr, typeTVariables, visitedPtrs);
            }
        }
        NPJ.heapAnalyze(typeTVariables, typeSVariables);
    }

    private void traverseFieldsOfVarT(int parentPtr, List<Integer> typeTVariables, Set<Integer> visitedPtrs) {
        int[] fFieldsPtrs = new int[] { parentPtr + 1, parentPtr + 2 };
        for(int ptr : fFieldsPtrs) {
            if(subVarShouldNotBeAdded(ptr, visitedPtrs)) {
                continue;
            }
            visitedPtrs.add(heap[ptr]);
            typeTVariables.add(heap[heap[ptr] + 3]);
            traverseFieldsOfVarT(heap[ptr], typeTVariables, visitedPtrs);
        }
    }

    private boolean varShouldNotBeAdded(int ptr, Set<Integer> visitedPtrs) {
        return ptr == NULL || visitedPtrs.contains(ptr);
    }

    private boolean subVarShouldNotBeAdded(int ptr, Set<Integer> visitedPtrs) {
        return ptr == NULL || visitedPtrs.contains(heap[ptr]);
    }

    private void allocateNewString(String varName, String value) {
        int valueLength = value.length();
        checkHeap(2 + valueLength);

        int allocPtr = collector.getAllocPtr();
        variables.put(varName, new ObjectMetadata(allocPtr, Type.S));
        heap[allocPtr++] = Type.S.code;
        heap[allocPtr++] = valueLength;
        for(char c : value.toCharArray()) {
            heap[allocPtr++] = c;
        }
        collector.setAllocPtr(allocPtr);
    }


    private String getString(int position) {
        int length = heap[++position];
        char[] storedChars = new char[length];
        ++position;
        for(int i = 0; i < length; ++i, ++position) {
            storedChars[i] = (char) heap[position];
        }
        return new String(storedChars);
    }

    private void checkHeap(int size) {
        if(collector.getFreeSpace(heap) < size) {
            NPJ.collect(heap, collector, (Map) variables);

            if(collector.getFreeSpace(heap) < size) {
                throw new RuntimeException("Heap overflow");
            }
        }
    }
}
