public class ObjectMetadata {
    public int ptr;
    public Type type;

    public ObjectMetadata(int ptr, Type type) {
        this.ptr = ptr;
        this.type = type;
    }
}