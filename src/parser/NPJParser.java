// Generated from NPJ.g4 by ANTLR 4.5.1
package parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class NPJParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		NULL=10, INT=11, STRING=12, WS=13;
	public static final int
		RULE_program = 0, RULE_statements = 1, RULE_statement = 2, RULE_varDecl = 3, 
		RULE_varDeclT = 4, RULE_varDeclSWithString = 5, RULE_varDeclSWithNull = 6, 
		RULE_assignment = 7, RULE_rvalue = 8, RULE_lvalue = 9, RULE_printStringInQuotes = 10, 
		RULE_printVarDeclS = 11, RULE_heapAnalyze = 12, RULE_collect = 13;
	public static final String[] ruleNames = {
		"program", "statements", "statement", "varDecl", "varDeclT", "varDeclSWithString", 
		"varDeclSWithNull", "assignment", "rvalue", "lvalue", "printStringInQuotes", 
		"printVarDeclS", "heapAnalyze", "collect"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'VarDeclT'", "'VarDeclS'", "'\"'", "'='", "'.'", "'Print'", 
		"'HeapAnalyze'", "'Collect'", "'NULL'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "NULL", "INT", 
		"STRING", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "NPJ.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public NPJParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			statements(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementsContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitStatements(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		return statements(0);
	}

	private StatementsContext statements(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		StatementsContext _localctx = new StatementsContext(_ctx, _parentState);
		StatementsContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_statements, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(31);
			statement();
			}
			_ctx.stop = _input.LT(-1);
			setState(37);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new StatementsContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_statements);
					setState(33);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(34);
					statement();
					}
					} 
				}
				setState(39);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public VarDeclContext varDecl() {
			return getRuleContext(VarDeclContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public PrintStringInQuotesContext printStringInQuotes() {
			return getRuleContext(PrintStringInQuotesContext.class,0);
		}
		public PrintVarDeclSContext printVarDeclS() {
			return getRuleContext(PrintVarDeclSContext.class,0);
		}
		public HeapAnalyzeContext heapAnalyze() {
			return getRuleContext(HeapAnalyzeContext.class,0);
		}
		public CollectContext collect() {
			return getRuleContext(CollectContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement);
		try {
			setState(58);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(40);
				varDecl();
				setState(41);
				match(T__0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				assignment();
				setState(44);
				match(T__0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(46);
				printStringInQuotes();
				setState(47);
				match(T__0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(49);
				printVarDeclS();
				setState(50);
				match(T__0);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(52);
				heapAnalyze();
				setState(53);
				match(T__0);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(55);
				collect();
				setState(56);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclContext extends ParserRuleContext {
		public VarDeclTContext varDeclT() {
			return getRuleContext(VarDeclTContext.class,0);
		}
		public VarDeclSWithStringContext varDeclSWithString() {
			return getRuleContext(VarDeclSWithStringContext.class,0);
		}
		public VarDeclSWithNullContext varDeclSWithNull() {
			return getRuleContext(VarDeclSWithNullContext.class,0);
		}
		public VarDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterVarDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitVarDecl(this);
		}
	}

	public final VarDeclContext varDecl() throws RecognitionException {
		VarDeclContext _localctx = new VarDeclContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_varDecl);
		try {
			setState(63);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(60);
				varDeclT();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(61);
				varDeclSWithString();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(62);
				varDeclSWithNull();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclTContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public VarDeclTContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclT; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterVarDeclT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitVarDeclT(this);
		}
	}

	public final VarDeclTContext varDeclT() throws RecognitionException {
		VarDeclTContext _localctx = new VarDeclTContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_varDeclT);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			match(T__1);
			setState(66);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclSWithStringContext extends ParserRuleContext {
		public List<TerminalNode> STRING() { return getTokens(NPJParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(NPJParser.STRING, i);
		}
		public VarDeclSWithStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclSWithString; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterVarDeclSWithString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitVarDeclSWithString(this);
		}
	}

	public final VarDeclSWithStringContext varDeclSWithString() throws RecognitionException {
		VarDeclSWithStringContext _localctx = new VarDeclSWithStringContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_varDeclSWithString);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			match(T__2);
			setState(69);
			match(STRING);
			setState(70);
			match(T__3);
			setState(71);
			match(STRING);
			setState(72);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclSWithNullContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public TerminalNode NULL() { return getToken(NPJParser.NULL, 0); }
		public VarDeclSWithNullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclSWithNull; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterVarDeclSWithNull(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitVarDeclSWithNull(this);
		}
	}

	public final VarDeclSWithNullContext varDeclSWithNull() throws RecognitionException {
		VarDeclSWithNullContext _localctx = new VarDeclSWithNullContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_varDeclSWithNull);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			match(T__2);
			setState(75);
			match(STRING);
			setState(76);
			match(NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public LvalueContext lvalue() {
			return getRuleContext(LvalueContext.class,0);
		}
		public RvalueContext rvalue() {
			return getRuleContext(RvalueContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitAssignment(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			lvalue(0);
			setState(79);
			match(T__4);
			setState(80);
			rvalue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RvalueContext extends ParserRuleContext {
		public LvalueContext lvalue() {
			return getRuleContext(LvalueContext.class,0);
		}
		public TerminalNode NULL() { return getToken(NPJParser.NULL, 0); }
		public TerminalNode INT() { return getToken(NPJParser.INT, 0); }
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public RvalueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rvalue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterRvalue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitRvalue(this);
		}
	}

	public final RvalueContext rvalue() throws RecognitionException {
		RvalueContext _localctx = new RvalueContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rvalue);
		try {
			setState(88);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(82);
				lvalue(0);
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 2);
				{
				setState(83);
				match(NULL);
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 3);
				{
				setState(84);
				match(INT);
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 4);
				{
				setState(85);
				match(T__3);
				setState(86);
				match(STRING);
				setState(87);
				match(T__3);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LvalueContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public LvalueContext lvalue() {
			return getRuleContext(LvalueContext.class,0);
		}
		public LvalueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lvalue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterLvalue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitLvalue(this);
		}
	}

	public final LvalueContext lvalue() throws RecognitionException {
		return lvalue(0);
	}

	private LvalueContext lvalue(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LvalueContext _localctx = new LvalueContext(_ctx, _parentState);
		LvalueContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_lvalue, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(91);
			match(STRING);
			}
			_ctx.stop = _input.LT(-1);
			setState(98);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new LvalueContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_lvalue);
					setState(93);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(94);
					match(T__5);
					setState(95);
					match(STRING);
					}
					} 
				}
				setState(100);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrintStringInQuotesContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public PrintStringInQuotesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_printStringInQuotes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterPrintStringInQuotes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitPrintStringInQuotes(this);
		}
	}

	public final PrintStringInQuotesContext printStringInQuotes() throws RecognitionException {
		PrintStringInQuotesContext _localctx = new PrintStringInQuotesContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_printStringInQuotes);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(T__6);
			setState(102);
			match(T__3);
			setState(103);
			match(STRING);
			setState(104);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintVarDeclSContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(NPJParser.STRING, 0); }
		public PrintVarDeclSContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_printVarDeclS; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterPrintVarDeclS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitPrintVarDeclS(this);
		}
	}

	public final PrintVarDeclSContext printVarDeclS() throws RecognitionException {
		PrintVarDeclSContext _localctx = new PrintVarDeclSContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_printVarDeclS);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(T__6);
			setState(107);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HeapAnalyzeContext extends ParserRuleContext {
		public HeapAnalyzeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_heapAnalyze; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterHeapAnalyze(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitHeapAnalyze(this);
		}
	}

	public final HeapAnalyzeContext heapAnalyze() throws RecognitionException {
		HeapAnalyzeContext _localctx = new HeapAnalyzeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_heapAnalyze);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollectContext extends ParserRuleContext {
		public CollectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).enterCollect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NPJListener ) ((NPJListener)listener).exitCollect(this);
		}
	}

	public final CollectContext collect() throws RecognitionException {
		CollectContext _localctx = new CollectContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_collect);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(T__8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return statements_sempred((StatementsContext)_localctx, predIndex);
		case 9:
			return lvalue_sempred((LvalueContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean statements_sempred(StatementsContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean lvalue_sempred(LvalueContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\17t\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3&\n"+
		"\3\f\3\16\3)\13\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\5\4=\n\4\3\5\3\5\3\5\5\5B\n\5\3\6\3\6\3\6\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\5\n[\n\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13c\n\13\f\13\16\13f\13\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\2\4\4\24\20"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\2\2q\2\36\3\2\2\2\4 \3\2\2\2\6<\3"+
		"\2\2\2\bA\3\2\2\2\nC\3\2\2\2\fF\3\2\2\2\16L\3\2\2\2\20P\3\2\2\2\22Z\3"+
		"\2\2\2\24\\\3\2\2\2\26g\3\2\2\2\30l\3\2\2\2\32o\3\2\2\2\34q\3\2\2\2\36"+
		"\37\5\4\3\2\37\3\3\2\2\2 !\b\3\1\2!\"\5\6\4\2\"\'\3\2\2\2#$\f\3\2\2$&"+
		"\5\6\4\2%#\3\2\2\2&)\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2(\5\3\2\2\2)\'\3\2\2"+
		"\2*+\5\b\5\2+,\7\3\2\2,=\3\2\2\2-.\5\20\t\2./\7\3\2\2/=\3\2\2\2\60\61"+
		"\5\26\f\2\61\62\7\3\2\2\62=\3\2\2\2\63\64\5\30\r\2\64\65\7\3\2\2\65=\3"+
		"\2\2\2\66\67\5\32\16\2\678\7\3\2\28=\3\2\2\29:\5\34\17\2:;\7\3\2\2;=\3"+
		"\2\2\2<*\3\2\2\2<-\3\2\2\2<\60\3\2\2\2<\63\3\2\2\2<\66\3\2\2\2<9\3\2\2"+
		"\2=\7\3\2\2\2>B\5\n\6\2?B\5\f\7\2@B\5\16\b\2A>\3\2\2\2A?\3\2\2\2A@\3\2"+
		"\2\2B\t\3\2\2\2CD\7\4\2\2DE\7\16\2\2E\13\3\2\2\2FG\7\5\2\2GH\7\16\2\2"+
		"HI\7\6\2\2IJ\7\16\2\2JK\7\6\2\2K\r\3\2\2\2LM\7\5\2\2MN\7\16\2\2NO\7\f"+
		"\2\2O\17\3\2\2\2PQ\5\24\13\2QR\7\7\2\2RS\5\22\n\2S\21\3\2\2\2T[\5\24\13"+
		"\2U[\7\f\2\2V[\7\r\2\2WX\7\6\2\2XY\7\16\2\2Y[\7\6\2\2ZT\3\2\2\2ZU\3\2"+
		"\2\2ZV\3\2\2\2ZW\3\2\2\2[\23\3\2\2\2\\]\b\13\1\2]^\7\16\2\2^d\3\2\2\2"+
		"_`\f\3\2\2`a\7\b\2\2ac\7\16\2\2b_\3\2\2\2cf\3\2\2\2db\3\2\2\2de\3\2\2"+
		"\2e\25\3\2\2\2fd\3\2\2\2gh\7\t\2\2hi\7\6\2\2ij\7\16\2\2jk\7\6\2\2k\27"+
		"\3\2\2\2lm\7\t\2\2mn\7\16\2\2n\31\3\2\2\2op\7\n\2\2p\33\3\2\2\2qr\7\13"+
		"\2\2r\35\3\2\2\2\7\'<AZd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}