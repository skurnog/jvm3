public enum Type {
    S(-1), T(-2);

    public final int code;

    Type(int code) {
        this.code = code;
    }
}