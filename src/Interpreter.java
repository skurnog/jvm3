import java.io.*;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import parser.NPJLexer;
import parser.NPJParser;

public class Interpreter {
    private static final String NPJ_HEAP_SIZE = "npj.heap.size";

    private InputStream programStream;
    private int[] heap;

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Segmentation fault. Kernel panic.");
            System.exit(1);
        }

        int heapSize = Integer.valueOf(System.getProperty(NPJ_HEAP_SIZE));
        Interpreter interpreter = new Interpreter(args[0], heapSize);
        try {
            interpreter.interpret();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(3);
        }
    }

    private Interpreter(String programFile, int heapSize) {
        programStream = getProgramStream(programFile);
        heap = new int[heapSize];
    }

    private void interpret() throws IOException {
        try {
            NPJLexer lexer = new NPJLexer(new ANTLRInputStream(programStream));
            TokenStream tokens = new CommonTokenStream(lexer);
            NPJParser parser = new NPJParser(tokens);
            parser.addParseListener(new NPJListenerImpl(new CollectorImpl(), heap));
            parser.program();
        } finally {
            programStream.close();
        }
    }

    private InputStream getProgramStream(String programFile) {
        InputStream programStream = null;
        try {
            programStream = new FileInputStream(programFile);
        } catch (FileNotFoundException e) {
            System.out.println("Could not open program file: " + programFile + ", exiting.");
            System.exit(2);
        }

        return programStream;
    }

    public int[] getHeap() {
        return heap;
    }

    public void setHeap(int[] heap) {
        this.heap = heap;
    }
}
