
import java.util.Map;

public class CollectorImpl implements Collector {

    private final int NULL = 0;

    private int allocPtr = 1;
    private boolean isFirstPart = true;

    @Override
    public void collect(int[] heap, Map<Object, Object> params) {
        Map<String, ObjectMetadata> variables = (Map) params;
        int newAllocPtr, scan;
        if(isFirstPart) {
            scan = newAllocPtr = heap.length / 2 + 1;
        } else {
            scan = newAllocPtr = 1;
        }

        ObjectMetadata info;
        int ptr, toCopy;
        Type type;
        for(Map.Entry<String, ObjectMetadata> entry : variables.entrySet()) {
            info = entry.getValue();
            ptr = info.ptr;
            if(ptr == NULL) {
                continue;
            }
            if(heap[ptr] > 0) {
                info.ptr = heap[ptr];
                continue;
            }
            type = info.type;
            toCopy = type == Type.T ? 4 : (2 + heap[ptr + 1]);
            System.arraycopy(heap, ptr, heap, newAllocPtr, toCopy);
            info.ptr = heap[ptr] = newAllocPtr;
            newAllocPtr += toCopy;
        }

        int[] fFields = new int[2];
        while(scan < newAllocPtr) {
            if(heap[scan] == Type.T.code) {
                fFields[0] = scan + 1;
                fFields[1] = scan + 2;
                for(int f : fFields) {
                    ptr = heap[f];
                    if(ptr == NULL) {
                        continue;
                    }
                    if(heap[ptr] > 0) {
                        heap[f] = heap[ptr];
                    } else {
                        System.arraycopy(heap, ptr, heap, newAllocPtr, 4);
                        heap[f] = heap[ptr] = newAllocPtr;
                        newAllocPtr += 4;
                    }
                }
                scan += 4;
            } else {
                scan += 2 + heap[scan + 1];
            }
        }

        allocPtr = newAllocPtr;
        isFirstPart = !isFirstPart;
    }

    public int getHeapStart(int[] heap) {
        return isFirstPart ? 1 : (heap.length / 2 + 1);
    }

    public int getFreeSpace(int[] heap) {
        return isFirstPart ? (heap.length / 2 + 1 - allocPtr) : (heap.length - allocPtr);
    }

    public int getAllocPtr() {
        return allocPtr;
    }

    public void setAllocPtr(int allocPtr) {
        this.allocPtr = allocPtr;
    }
}
